experiment_folder=${PWD}"/neurorobin"

engine_outside_folder=${PWD}"/opensim_engine/OpensimLib.py"
engine_inside_folder="/home/nrpuser/.local/nrp/lib/python3.8/site-packages/nrp_core/engines/py_sim/OpensimLib.py"

manager_outside_folder=${PWD}"/opensim_engine/SimManager.py"
manager_inside_folder="/home/nrpuser/.local/nrp/lib/python3.8/site-packages/nrp_core/engines/py_sim/SimManager.py"

tvb_outside_location=${PWD}"/tvb/stroke_connectome.zip"
tvb_inside_location="/home/nrpuser/tvb-data/tvb_data/connectivity/stroke_connectome.zip"

sudo docker pull hbpneurorobotics/nrp-opensim-tvb-ubuntu20-demo:latest
xhost +si:localuser:root
		
sudo docker run -ti \
        --network=host \
        -e DISPLAY=$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v ${experiment_folder}:"/nrp-templates/neurorobin" \
		-v ${engine_outside_folder}:${engine_inside_folder} \
		-v ${manager_outside_folder}:${manager_inside_folder} \
		-v ${tvb_outside_location}:${tvb_inside_location} \
        --entrypoint "/bin/bash" -w "/nrp-templates/neurorobin"\
		--privileged\
        hbpneurorobotics/nrp-opensim-tvb-ubuntu20-demo:latest

#


