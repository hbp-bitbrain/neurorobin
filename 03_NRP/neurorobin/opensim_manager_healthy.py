from nrp_core.engines.opensim import OpenSimEngineScript
from nrp_core.engines.python_json import RegisterEngine

# The API of Opensim is shown in the following link:
# https://simtk.org/api_docs/opensim/api_docs

@RegisterEngine()
class Script(OpenSimEngineScript):
    def initialize(self):
        """Initialize datapack1 with time"""
        print("Server Engine is initializing")
        print("Registering datapack --> for sensors")
        self._registerDataPack("joints")
        self._setDataPack("joints", {"EM_tz": 0, "ET_z":0, "EF_y":0})
        self._registerDataPack("infos")
        self._setDataPack("infos", {"time": 0})

        # To set the force of muscles, in arm_26, they are:
        # ['TRIlong', 'TRIlat', 'TRImed', 'BIClong', 'BICshort', 'BRA']
        # The default color of muscle in the visualizer is blue.
        # Once the force of a muscle is not the default value, 
        # the color of the muscle will be changed. 
        # Using this phenomenon, the controlled muscles can be found in the visualizer
        # For example, if action=[0.5, 0.0, 0.0, 0.0, 0.0, 0.0], 
        # the color of TRIlong will not be blue in shown screen
        #self.action = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        # Actuators in the model are: 
        # ['pect_maj_c_1', 'lat_dorsi_2', 'delt_scap_4', 'delt_scap_8', 'delt_scap11',
        #  'delt_clav_3', 'tric_long_3', 'tric_med_2', 'bic_l', 'bic_b_1',
        #  'brachialis_2', 'brachiorad_2', 'pron_teres_1', 'supinator_2', 'tric_lat_3',
        #  'FCR', 'FCU', 'ECU', 'ECRL', 'FDSI',
        #  'FDSM', 'FDSR', 'FDSL', 'EDCL', 'EDCR',
        #  'EDCM', 'EDCI', 'APL', 'ADP', 'EPB',
        #  'FPB', 'exo_base_motor', 'exo_fingers_motor', 'exo_thumb_motor', 'exo_avance',
        #  'exo_pronosup', 'exo_fingerflex', 'exo_fingerext']

        self.sim_manager


        self.action = [0.1,0.1,0.1,0.1,0.1,
            0.1,0.1,0.1,0.1,0.1,
            0.1,0.1,0.1,0.1,0.1,
            0.1,0.1,0.1,0.1,0.1,
            0.1,0.1,0.1,0.1,0.1,
            0.1,0.1,0.1,0.1,0.1,
            0.1,0,0,0,0,
            0,0,0]

        print("Registering datapack --> for actuators")
        self._registerDataPack("control_cmd")
        self._registerDataPack("action")
        self._setDataPack("action", { "act_list": self.action, "reset": 0})
        
        
        
        ################# CONTROL ##########################
        self.state=0 #0: go forward; 1: open hand; 2: close hand
        self.e_i = 0
        self.e_old = 0

        self.count = 0
        self.u = 0
        self.kp = 100#0.39
        self.ki = 0 #0.001
        self.kd = 0#0.99

        self.reset_count = 0
        
        
        joints = self._getDataPack("joints")
        exo_forward_value = joints.get("EM_tz")
        exo_thunb_value = joints.get("ET_z")
        exo_fingers_value = joints.get("EF_y")
        self.initial_point=exo_forward_value ##### TAKE ZERO!!! #### INICIA EN -0.3813
        self.final_point=-0.43#back: -0.43#forward: -0.31
        self.speed_target=0.1 #meters/sec
        self.step_period_sec=0.001

        min_rom=-0.59999999999999998 
        max_rom=0
        force_actuator=1000

        
        
        self.speed_old=0
        self.exo_base_position_old = self.initial_point
        self.exo_thumb_position_old = self.initial_point
        self.exo_fingers_position_old = self.initial_point
        
        
        ##### SEQUENCE TO DO ######
        self.list_final_points_base=[-0.31, -0.43, -0.31, -0.43, -0.31, -0.43]
        self.list_final_points_thumb=[-0.46, -0.9]#[-95, -44, -95, -44, -95, -44, -95]
        self.list_final_points_fingers=[-0.6, 0.1]#[-17, 17, -17, 17, -17, 17, -17]
        
        #PID Base
        self.initial_point_base=0
        self.final_point_base=0
        self.speed_old_base=0
        self.speed_target_base=0.1
        self.e_i_base = 0
        self.e_old_base = 0
        self.u_base = 0
        self.kp_base = 0.2#0.39
        self.ki_base = 0 #0.001
        self.kd_base = 0#0.99
        
        #PID Thumb
        self.initial_point_thumb=0
        self.final_point_thumb=0
        self.speed_old_thumb=0
        self.speed_target_thumb=0.1
        self.e_i_thumb = 0
        self.e_old_thumb = 0
        self.u_thumb = 0
        self.kp_thumb = 0.2#0.39
        self.ki_thumb = 0 #0.001
        self.kd_thumb = 0#0.99
        
        #PID Fingers
        self.initial_point_fingers=0
        self.final_point_fingers=0
        self.speed_old_fingers=0
        self.speed_target_fingers=0.1
        self.e_i_fingers = 0
        self.e_old_fingers = 0
        self.u_fingers = 0
        self.kp_fingers = 0.4#0.39
        self.ki_fingers = 0 #0.001
        self.kd_fingers = 0#0.99
        
        #################################################

    def runLoop(self, timestep):
        # Receive control data from TF
        self.action = self._getDataPack("action").get("act_list")
        reset_falg = self._getDataPack("control_cmd").get("reset")

        # Actuators in the model are: 
        # ['pect_maj_c_1', 'lat_dorsi_2', 'delt_scap_4', 'delt_scap_8', 'delt_scap11',
        #  'delt_clav_3', 'tric_long_3', 'tric_med_2', 'bic_l', 'bic_b_1',
        #  'brachialis_2', 'brachiorad_2', 'pron_teres_1', 'supinator_2', 'tric_lat_3',
        #  'FCR', 'FCU', 'ECU', 'ECRL', 'FDSI',
        #  'FDSM', 'FDSR', 'FDSL', 'EDCL', 'EDCR',
        #  'EDCM', 'EDCI', 'APL', 'ADP', 'EPB',
        #  'FPB', 'exo_base_motor', 'exo_fingers_motor', 'exo_thumb_motor', 'exo_avance',
        #  'exo_pronosup', 'exo_fingerflex', 'exo_fingerext']

        #self.action = [0,0,0,0,0,
        #    0,0,0,0,0,
        #    0,0,0,0,0,
        #    0,0,0,0,0,
        #    0,0,0,0,0,
        #    0,0,0,0,0,
        #    0,-1.782,0.01,0.01,0,
        #    0,0,0]

    
        #print(self.sim_manager.get_model_properties("Force"))
        
        
        
        ################### CONTROL ####################
        
        joints = self._getDataPack("joints")
        
        #PRINT ALL THE VARIABLES
        print("HU_x: " + str(joints.get("HU_x")))
        print("HU_z: " + str(joints.get("HU_z")))
        print("HU_yy: " + str(joints.get("HU_yy")))
        print("EL_x: " + str(joints.get("EL_x")))
        print("EL_yy: " + str(joints.get("EL_yy")))
        print("MTF_z: " + str(joints.get("MTF_z")))
        print("EM_tz: " + str(joints.get("EM_tz")))
        print("EBB_tx: " + str(joints.get("EBB_tx")))
        print("EBH_ty: " + str(joints.get("EBH_ty")))
        print("ET_z: " + str(joints.get("ET_z")))
        print("EF_y: " + str(joints.get("EF_y")))

        
        exo_base_position = joints.get("EM_tz")
        exo_thumb_position = joints.get("ET_z")
        exo_fingers_position = joints.get("EF_y")
        print(self.kp_base)
        if(self.reset_count==1):
            # EXO BASE INITIALIZATION
            self.initial_point_base=exo_base_position
            self.final_point_base=self.list_final_points_base[0]
            self.exo_base_position_old = self.initial_point_base
            self.e_i_base = 0
            self.e_old_base = 0
            self.u_base = 0
            self.kp_base = 0.2#0.39
            self.ki_base = 0 #0.001
            self.kd_base = 0#0.99
            if(self.final_point_base-self.initial_point_base)<=0:
            	self.speed_target_base=-self.speed_target_base
            	self.kp_base = 0.8#0.39
            	self.ki_base = -0.8 #0.001
            	self.kd_base = 0#0.99
            
            # EXO THUMB INITIALIZATION
            self.initial_point_thumb=exo_thumb_position
            self.final_point_thumb=self.list_final_points_thumb[0]
            self.exo_thumb_position_old = self.initial_point_thumb
            self.e_i_thumb = 0
            self.e_old_thumb = 0
            self.u_thumb = 0
            self.kp_thumb = 0.2#0.39
            self.ki_thumb = 0 #0.001
            self.kd_thumb = 0#0.99
            if(self.final_point_thumb-self.initial_point_thumb)<=0:
            	self.speed_target_thumb=-self.speed_target_thumb
            	self.kp_thumb = 0.2#0.39
            	self.ki_thumb = 0 #0.001
            	self.kd_thumb = 0#0.99
            
            # EXO FINGERS INITIALIZATION
            self.initial_point_fingers=exo_fingers_position
            self.final_point_fingers=self.list_final_points_fingers[0]
            self.exo_fingers_position_old = self.initial_point_fingers
            self.e_i_fingers = 0
            self.e_old_fingers = 0
            self.u_fingers = 0
            self.kp_fingers = 0.2#0.39
            self.ki_fingers = 0 #0.001
            self.kd_fingers = 0#0.99
            if(self.final_point_fingers-self.initial_point_fingers)<=0:
            	self.speed_target_fingers=-self.speed_target_fingers
            	self.kp_fingers = 0.8#0.39
            	self.ki_fingers = -0.8 #0.001
            	self.kd_fingers = 0#0.99
        

        speed_actual_base=(exo_base_position-self.exo_base_position_old)/self.step_period_sec
        self.exo_base_position_old=exo_base_position
        print(exo_base_position)
        speed_actual_thumb=(exo_thumb_position-self.exo_thumb_position_old)/self.step_period_sec
        #print("EXO_THUMB_POSITION")
        #print(exo_thumb_position)
        #print("EXO_FINGERS_POSITION")
        #print(exo_fingers_position)
        #print("EXO_THUMB_PSOITION_OLD")
        #print(self.exo_thumb_position_old)
        #print("Speed")
        self.exo_thumb_position_old=exo_thumb_position
        speed_actual_fingers=(exo_fingers_position-self.exo_fingers_position_old)/self.step_period_sec
        self.exo_fingers_position_old=exo_fingers_position
        #print(speed_actual_thumb)
        #print(self.speed_target_thumb)
        if True:#self.count % 4 == 0:
            err = speed_actual_base - self.speed_target_base
            err = -err
            self.u_base = self.pid_ctrl_base(err)
            err = speed_actual_thumb - self.speed_target_thumb
            #err = -err
            self.u_thumb = self.pid_ctrl_thumb(err)
            if(self.u_thumb>100):
            	self.u_thumb=100
            elif(self.u_thumb<-100):
            	self.u_thumb=-100
            err = speed_actual_fingers - self.speed_target_fingers
            err = -err
            self.u_fingers = self.pid_ctrl_fingers(err)
        self.count = self.count + 1
        
        """
        if(self.state==0): #Go forward
            if(self.final_point_base-self.initial_point_base)<=0:
            	self.action[31]=-5+self.u_base
            else:
            	self.action[31] = 10-self.u_base
        elif(self.state==1): #Open Hand
            if(self.final_point_thumb-self.initial_point_thumb)<=0:
            	self.action[33]= -0.001#10-self.u_thumb
            else:
            	self.action[33] = -0.001#-5+self.u_thumb
		
            if(self.final_point_fingers-self.initial_point_fingers)<=0:
            	self.action[32]= -200#-5+self.u_fingers
            else:
            	self.action[32] = +200#10-self.u_fingers
        """   
        #'''
        reset_flag = 0
        self.reset_count = self.reset_count + 1
        
        """
        # The reset need two step: one step to start the reset
        # and one step to wait the end of reset
        if self.reset_count % 200 < 2: 
            if self.reset_count % 200 == 0:
                reset_flag = 1
            self.action = [0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,0,0,0,0,
            0,-15,0,0,0,
            0,0,0]
            #self.para_init()
        """




        if self.state==0:
        	if exo_base_position>self.final_point_base:
        		print("Abrir mano")
        		self.state=1 #Abrir mano
        	self.action[31]=10-self.u_base
        	#self.action[31]=30000
        	if self.state>0:
        		self.action[31]=10
        if self.state==1:
        	print("Abriendo mano")
        	if exo_fingers_position<self.final_point_fingers:
        		print(exo_fingers_position)
        		print(self.final_point_fingers)
        		print("Cerrar mano")
        		self.state=2 #Cerrar mano
        		self.final_point_fingers=self.list_final_points_fingers[1]
        	self.action[33]= -0.002#0.001
        	self.action[32]= -200
        if self.state==2:
        	print("Cerrando mano")
        	if exo_fingers_position>self.final_point_fingers:
        		print("Abrir mano")
        		self.state=1 #Abrir mano
        		self.final_point_fingers=self.list_final_points_fingers[0]
        	#self.action[33]= 0.001
        	self.action[32]= 0.1
        	
        	
        """
        if(self.final_point_base-self.initial_point_base)>0 and exo_base_position>self.final_point_base:# and exo_thumb_position>self.final_point_thumb and exo_fingers_position>self.final_point_fingers:
            print("FORWARD REACH")
            if(len(self.list_final_points_base)>1):
            	#Base
            	self.final_point_base=self.list_final_points_base[1]
            	self.initial_point_base=self.list_final_points_base[0]
            	del self.list_final_points_base[0]
            	self.speed_target_base=-self.speed_target_base
            	self.e_i_base = 0
            	self.e_old_base = 0
            	if(self.final_point_base-self.initial_point_base)<=0:
            		self.speed_target_base=-self.speed_target_base
            		self.kp_base = 0.8#0.39
            		self.ki_base = -0.8 #0.001
            		self.kd_base = 0#0.99
            	else:
            		self.speed_target_base=-self.speed_target_base
            		self.kp_base = 0.2
            		self.ki_base = 0
            		self.kd_base = 0
            	
            	#Thumb
            	self.final_point_thumb=self.list_final_points_thumb[1]
            	self.initial_point_thumb=self.list_final_points_thumb[0]
            	del self.list_final_points_thumb[0]
            	self.speed_target_thumb=-self.speed_target_thumb
            	self.e_i_base = 0
            	self.e_old_base = 0
            	if(self.final_point_thumb-self.initial_point_thumb)<=0:
            		self.speed_target_thumb=-self.speed_target_thumb
            		self.kp_thumb = 0.8#0.39
            		self.ki_thumb = -0.8 #0.001
            		self.kd_thumb = 0#0.99
            	else:
            		self.speed_target_thumb=-self.speed_target_thumb
            		self.kp_thumb = 0.2
            		self.ki_thumb = 0
            		self.kd_thumb = 0
            	
            	#Fingers
            	self.final_point_fingers=self.list_final_points_fingers[1]
            	self.initial_point_fingers=self.list_final_points_fingers[0]
            	del self.list_final_points_fingers[0]
            	self.speed_target_fingers=-self.speed_target_fingers
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	if(self.final_point_fingers-self.initial_point_fingers)<=0:
            		self.speed_target_fingers=-self.speed_target_fingers
            		self.kp_fingers = 0.8#0.39
            		self.ki_fingers = -0.8 #0.001
            		self.kd_fingers = 0#0.99
            	else:
            		self.speed_target_fingers=-self.speed_target_fingers
            		self.kp_fingers = 0.2
            		self.ki_fingers = 0
            		self.kd_fingers = 0
            	
            else:
            	print("FIN1")
            	self.action = [0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0]
        elif(self.final_point_base-self.initial_point_base)<=0 and exo_base_position<self.final_point_base:
            print("BACKWARD REACH")
            if(len(self.list_final_points_base)>1):
            	#Base
            	self.final_point_base=self.list_final_points_base[1]
            	self.initial_point_base=self.list_final_points_base[0]
            	del self.list_final_points_base[0]
            	self.speed_target_base=-self.speed_target_base
            	self.e_i_base = 0
            	self.e_old_base = 0
            	if(self.final_point_base-self.initial_point_base)<=0:
            		self.speed_target_base=-self.speed_target_base
            		self.kp_base = 0.8#0.39
            		self.ki_base = -0.8 #0.001
            		self.kd_base = 0#0.99
            	else:
            		self.speed_target_base=-self.speed_target_base
            		self.kp_base = 0.2
            		self.ki_base = 0
            		self.kd_base = 0
            	
            	#Thumb
            	self.final_point_thumb=self.list_final_points_thumb[1]
            	self.initial_point_thumb=self.list_final_points_thumb[0]
            	del self.list_final_points_thumb[0]
            	self.speed_target_thumb=-self.speed_target_thumb
            	self.e_i_base = 0
            	self.e_old_base = 0
            	if(self.final_point_thumb-self.initial_point_thumb)<=0:
            		self.speed_target_thumb=-self.speed_target_thumb
            		self.kp_thumb = 0.8#0.39
            		self.ki_thumb = -0.8 #0.001
            		self.kd_thumb = 0#0.99
            	else:
            		self.speed_target_thumb=-self.speed_target_thumb
            		self.kp_thumb = 0.2
            		self.ki_thumb = 0
            		self.kd_thumb = 0
            	
            	#Fingers
            	self.final_point_fingers=self.list_final_points_fingers[1]
            	self.initial_point_fingers=self.list_final_points_fingers[0]
            	del self.list_final_points_fingers[0]
            	self.speed_target_fingers=-self.speed_target_fingers
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	if(self.final_point_fingers-self.initial_point_fingers)<=0:
            		self.speed_target_fingers=-self.speed_target_fingers
            		self.kp_fingers = 0.8#0.39
            		self.ki_fingers = -0.8 #0.001
            		self.kd_fingers = 0#0.99
            	else:
            		self.speed_target_fingers=-self.speed_target_fingers
            		self.kp_fingers = 0.2
            		self.ki_fingers = 0
            		self.kd_fingers = 0
            else:
            	print("FIN2")
            	self.action = [0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0,0,0,
            	0,0,0]
        """
        self._setDataPack("action", { "act_list": self.action, "reset": reset_flag})
        
        ################################################
        
        
        

        if reset_falg == 1:
            print("RESET FALG")
            self.reset()
        else:
            # All Joints and Muscles can be found in the "*.osim"
            # Obtain the joint data from model "arm_26"
            # In arm_26, the joint set is [offset, r_shoulder, r_elbow]
            exo_forward_value = self.sim_manager.get_model_property("exotable1", datapack_type="Joint")
            exo_thumb_value = self.sim_manager.get_model_property("exothumb", datapack_type="Joint")
            exo_fingers_value = self.sim_manager.get_model_property("exomotorfingers", datapack_type="Joint")
            # Send data to TF
            self._setDataPack("joints", {"EM_tz": exo_forward_value, "ET_z": exo_thumb_value, "EF_y": exo_fingers_value})
            self._setDataPack("infos", {"time": self.sim_manager.get_sim_time()})
        # Set muscles' force to change joints
        self.sim_manager.run_step(self.action)
        # To show components in the model changed by action
        # 1: To show components in a list
        #ctrl_list = self.sim_manager.theWorld.model.getControlsTable()
        # 2: To show components one by one
        #print(self.sim_manager.get_model_properties("Force"))

    def reset(self):
        print("resetting the opensim simulation...")
        # Reset the value of set datapacks
        self._setDataPack("joints", {"EM_tz": 0, "ET_z":0, "EF_y":0})
        self._setDataPack("infos", {"time": 0})
        # Reset simulation model
        self.sim_manager.reset()
        
    def shutdown(self):
        print("Engine 1 is shutting down")
        
    """
    def para_init(self):
        self.e_i_base = 0
        self.e_old_base = 0

        self.count = 0
        self.u_base = 0
        self.kp_base = 0.39
        self.ki_base = 0.001
        self.kd_base = 0.99
    """

    def pid_ctrl_base(self, error):
        self.e_i_base = self.e_i_base + error
        de = error - self.e_old_base
        u = self.kp_base*error + self.ki_base*self.e_i_base  + self.kd_base*de
        self.e_old_base = error

        return u
        
    def pid_ctrl_thumb(self, error):
        self.e_i_thumb = self.e_i_thumb + error
        de = error - self.e_old_thumb
        u = self.kp_thumb*error + self.ki_thumb*self.e_i_thumb  + self.kd_thumb*de
        self.e_old_thumb = error

        return u
        
        
    def pid_ctrl_fingers(self, error):
        self.e_i_fingers = self.e_i_fingers + error
        de = error - self.e_old_fingers
        u = self.kp_fingers*error + self.ki_fingers*self.e_i_fingers  + self.kd_fingers*de
        self.e_old_fingers = error

        return u


