from nrp_core import *
from nrp_core.data.nrp_json import *

import ranges

@EngineDataPack(keyword='os_muscle_lengths', id=DataPackIdentifier('os_muscle_lengths', 'opensim'))
@EngineDataPack(keyword='os_muscle_velocities', id=DataPackIdentifier('os_muscle_velocities', 'opensim'))
@EngineDataPack(keyword='os_muscle_forces', id=DataPackIdentifier('os_muscle_forces', 'opensim'))

@TransceiverFunction("tvb")

def transceiver_function(os_muscle_lengths,os_muscle_velocities,os_muscle_forces):
    #rec_joints = JsonDataPack("joint_data", "tvb")
    #rec_joints.data["exotable1"] = joints.data["EM_tz"]
    #rec_joints.data["exothumb"] = joints.data["ET_z"]
    #rec_joints.data["exomotorfingers"] = joints.data["EF_y"]
    #rec_joints.data["elbow"] = ranges.opensim_displacement_to_tvb_voltage(joints.data["elbow"])
    
    type_Ia = JsonDataPack("type_Ia","tvb")
    type_II = JsonDataPack("type_II","tvb")
    type_Ib = JsonDataPack("type_Ib","tvb")

    type_Ia.data["frequencies"] = ranges.type_Ia_transformation(velocities = os_muscle_velocities.data["velocity_list"], 
                                                                lengths = os_muscle_lengths.data["length_list"])
    type_II.data["frequencies"] = ranges.type_II_transformation(lengths = os_muscle_lengths.data["length_list"])
    type_Ib.data["frequencies"] = ranges.type_Ib_transformation(forces = os_muscle_forces.data["force_list"])
    
    return [type_Ia,type_II,type_Ib]

