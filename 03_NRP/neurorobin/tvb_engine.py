"""Python Engine 1. Will get current engine time and make it accessible as a datapack"""

import os
import inspect
import numpy as np
from copy import deepcopy
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from tvb.basic.profile import TvbProfile
from tvb.contrib.cosimulation.cosim_monitors import CosimCoupling
from tvb.simulator.models.oscillator import Generic2dOscillator
from tvb.datatypes.connectivity import Connectivity
from tvb.contrib.cosimulation.cosimulator import CoSimulator as Simulator
from tvb.simulator.coupling import Linear
from tvb.simulator.integrators import EulerStochastic
from tvb.simulator.monitors import Raw

import tvb_data

from nrp_core.engines.python_json import EngineScript

TvbProfile.set_profile(TvbProfile.LIBRARY_PROFILE)

TVB_DATA_PATH = os.path.dirname(inspect.getabsfile(tvb_data))

DEFAULT_SUBJECT_PATH = os.path.join(TVB_DATA_PATH, "connectivity")
DEFAULT_CONNECTIVITY_ZIP = os.path.join(DEFAULT_SUBJECT_PATH, "stroke_connectome.zip")

DEFAULT_SUBJECT_PATH = os.path.join(TVB_DATA_PATH, "berlinSubjects", "QL_20120814")
DEFAULT_CONNECTIVITY_ZIP = os.path.join(DEFAULT_SUBJECT_PATH, "QL_20120814_Connectivity.zip")

DT = 0.1  # Time step of integration
SPEED = 3.0 # Speed of transmission of brain signals
# Global coupling scaling for non-task-specific connectivity weights
G = 0.25  # G=0.25 works, G>=0.5 destabilizes the integration
NOISE = 0.0001  # Noise std level


class Script(EngineScript):

    def _initialize_connectivity(self):
        proxy_index = None
        MIN_TRACT_LENGTH = DT * SPEED

        # Human connectivity:

        connectivity = Connectivity.from_file(DEFAULT_CONNECTIVITY_ZIP)
        number_of_regions = connectivity.weights.shape[0]
        regions_labels = connectivity.region_labels.tolist()

        print(regions_labels)        
        
        motor_regions = np.array(['precentral_L'])
        sensory_regions = np.array(['postcentral_L'])
        regions_labels = connectivity.region_labels.tolist()

        iM = np.array([regions_labels.index(mr) for mr in motor_regions])
        iS = np.array([regions_labels.index(mr) for mr in sensory_regions])

        # Make sure that we don't get 0 delays for existing connections
        connectivity.tract_lengths = np.maximum(connectivity.tract_lengths, MIN_TRACT_LENGTH)
        # Normalize connectome:
        connectivity.weights = connectivity.scaled_weights(mode="region")
        connectivity.weights /= np.percentile(connectivity.weights, 99)
        connectivity.weights *= G   # Downscale non-task-specific connectome

        # Inject the proxy region into the brain

        proxy_region_names = ['pect_maj_c_1', 'lat_dorsi_2', 'delt_scap_4', 'delt_scap_8', 'delt_scap11',
          'delt_clav_3', 'tric_long_3', 'tric_med_2', 'bic_l', 'bic_b_1',
          'brachialis_2', 'brachiorad_2', 'pron_teres_1', 'supinator_2', 'tric_lat_3',
          'FCR', 'FCU', 'ECU', 'ECRL', 'FDSI',
          'FDSM', 'FDSR', 'FDSL', 'EDCL', 'EDCR',
          'EDCM', 'EDCI', 'APL', 'ADP', 'EPB',
          'FPB']
                
        # Create proxy regions for the muscle regions
        n_proxy_regions = len(proxy_region_names)
        connectivity.region_labels = np.concatenate([connectivity.region_labels,
                                                    np.array(proxy_region_names)])
        number_of_regions = connectivity.region_labels.shape[0]

        proxy_region_labels = np.arange(1, n_proxy_regions + 1)
        iF = number_of_regions - np.array(proxy_region_labels).astype('i')

        maxCentres = np.max(connectivity.centres, axis=0)
        connectivity.centres = np.concatenate([connectivity.centres,
                                            -maxCentres[np.newaxis],
                                            maxCentres[np.newaxis]])
        connectivity.areas = np.concatenate([connectivity.areas,
                                            np.array([connectivity.areas.min()]*n_proxy_regions)])
        connectivity.hemispheres = np.concatenate([connectivity.hemispheres,
                                                np.array([True]*n_proxy_regions)])
        connectivity.cortical = np.concatenate([connectivity.cortical,
                                                np.array([False]*n_proxy_regions)])
        for attr, val in zip(["weights", "tract_lengths"], [1.0, MIN_TRACT_LENGTH]):
            prop = getattr(connectivity, attr).copy()
            prop = np.concatenate([prop, np.zeros((number_of_regions-n_proxy_regions, n_proxy_regions))], axis=1)
            prop = np.concatenate([prop, np.zeros((n_proxy_regions, number_of_regions))], axis=0)
            # Connect the unilateral motor & sensory hemispheres to the muscles
                      
            prop[iF[0], iM[0]] = val  # Motor region -> Muscle 
            prop[iS[0], iF[0]] = val  # Muscle -> Motor region
            setattr(connectivity, attr, prop)

        connectivity.speed = np.array([SPEED])
        connectivity.configure()
        self.connectivity = connectivity
        self.iM = iM
        self.iF = iF
        #self.iF = np.array(proxy_index)

    def _initialize_simulator(self):
        # Create simulator, connectivity, linear coupling, integrator with noise, monitors
        simulator = Simulator()

        simulator.connectivity = self.connectivity

        simulator.coupling = Linear(a=np.array([1.0]))

        simulator.model = Generic2dOscillator()
        simulator.model.cvar = np.array([0, 1], dtype=np.int32)


        simulator.integrator = EulerStochastic()
        simulator.integrator.dt = DT
        simulator.integrator.noise.nsig = np.array([NOISE] * simulator.model.nvar)

        mon_raw = Raw(period=1.0)  # ms
        simulator.monitors = (mon_raw, )

        # Set initial conditions appropriate for inphase and antiphase coordination mode:
        simulator.connectivity.set_idelays(simulator.integrator.dt)
        simulator.horizon = simulator.connectivity.idelays.max() + 1  # time horizon in the past due to delays
        simulator.initial_conditions = 0.1*np.ones((simulator.horizon,
                                                    simulator.model.nvar,
                                                    self.connectivity.number_of_regions,
                                                    1))

        simulator.synchronization_time = DT

        # Set CoSim simulator parameters

        simulator.voi = np.array([0, 1])  # State variables to be interchanged with the arm's cosimulator
        simulator.proxy_inds = self.iF    # The nodes simulated by the arm's cosimulator

        # Coupling from all TVB nodes, towards the nodes of the arm's cosimulator
        # is what will be transferred from TVB to the fingers:
        simulator.cosim_monitors = (CosimCoupling(), )
        simulator.cosim_monitors[0].coupling.a = np.array([1.0])
        simulator.exclusive = True  # Arm is exclusively simulated by the arm's cosimulator (opensim)

        # Configure the simulator

        simulator.configure()

        # Enable interactive mode
        plt.ion()

        # Create figure and axis
        self.fig, (self.ax, self.ax2) = plt.subplots(nrows=2, ncols=1)
        
        self.ax.set_xlim(0, 98)  # Adjust as needed
        self.ax.set_ylim(-1, 1)  # Adjust as needed

        self.fig2, self.ax3 = plt.subplots()

        # Initialize a line plot
        #self.line, = self.ax.plot([], [], lw=2)

        print(simulator)
        print(simulator.model)

        self.simulator = simulator

    def _initialize_tvb(self):
        self._initialize_connectivity()
        self._initialize_simulator()

    def _initialize_datapacks(self):
        self._registerDataPack("action")
        self._registerDataPack("joint_data")

        self._registerDataPack("type_Ia")
        self._registerDataPack("type_II")
        self._registerDataPack("type_Ib")

        self.frequencies = [self.init_frequency] * 31
        self.action = [self.init_position] * 6

        self._setDataPack("action", { "act_list": self.action, "reset": 0})
        self._setDataPack("type_Ia", { "frequencies": self.frequencies})
        self._setDataPack("type_II", { "frequencies": self.frequencies})
        self._setDataPack("type_Ib", { "frequencies": self.frequencies})

    def initialize(self):
        self.init_position = -2.0
        self.init_frequency = 0.0
        self._initialize_tvb()
        self._initialize_datapacks()
        self.time_ms = 0
        self.iteration = 0
        self.prev_position = None
        self.target = []
        self.w = []
        self.prev_state = [np.array([self.simulator.current_step * self.simulator.integrator.dt]), # First and last times of the synchronization time
                                     self.simulator.initial_conditions[-1:, :, self.iF, :]]

    def simulate_fun(self, simulator, type_Ia, type_II, type_Ib):
        
        motor_commands_data = deepcopy(simulator.loop_cosim_monitor_output()[0])
        motor_commands_data[1] = motor_commands_data[1][:, :, self.iF, :]

        input = deepcopy(self.prev_state)

        # Inject arm's position into the brain
        # Meaning of indexes - [time = 0, data = 1][sample, state variable (V, W), brain region, mode (not used, always 0)]
        # Remove the scaling factor (5) to see a change in the model's behaviour

        scaling_factor = 1
        
        for i in range(len(type_II['frequencies'])):
            np.size(input[1])
            input[1][0, 0, i, 0] = type_II['frequencies'][i] * scaling_factor

        for i in range(len(type_Ia['frequencies'])):
            input[1][0, 1, i, 0] = type_Ia['frequencies'][i] * scaling_factor

        # We want to inject the data 'in the past'
        input[0][0] -= simulator.integrator.dt
        
        # Simulate the brain

        dtres = list(simulator(cosim_updates=input))[0]

        sim_res = list(dtres)
        # For a single time point, correct the 1st dimension:
        sim_res[0][0] = np.array([sim_res[0][0]])
        sim_res[0][1] = sim_res[0][1][np.newaxis]

        self.prev_state[0] = deepcopy(motor_commands_data[0])
        self.prev_state[1] = deepcopy(motor_commands_data[1])

        return motor_commands_data, sim_res


    def save_results(self, tempres, afferent_fibres_dict):
        if self.time_ms == 0:
            self.results = list(tempres)
        else:
            self.results[0][0] = np.concatenate([self.results[0][0], tempres[0][0]])
            self.results[0][1] = np.concatenate([self.results[0][1], tempres[0][1]])
            muscle_names = ['pect_maj_c_1', 'lat_dorsi_2', 'delt_scap_4', 'delt_scap_8', 'delt_scap11',
                            'delt_clav_3', 'tric_long_3', 'tric_med_2', 'bic_l', 'bic_b_1','brachialis_2', 
                            'brachiorad_2', 'pron_teres_1', 'supinator_2', 'tric_lat_3', 'FCR', 'FCU', 
                            'ECU', 'ECRL', 'FDSI','FDSM', 'FDSR', 'FDSL', 'EDCL', 'EDCR','EDCM', 'EDCI', 
                            'APL', 'ADP', 'EPB','FPB']
            
            self.plot_brain_region_activity(['precentral_L', 'postcentral_L'],  muscle_names[18:])
            self.plot_afferent_fibers_activity(afferent_fibres_dict, muscle_names)

    def plot_afferent_fibers_activity(self, afferent_fibres_dict, muscle_names):

        self.ax3.clear()

        # Set labels and titles as needed
        self.ax3.set_xlabel('Muscle names')
        self.ax3.set_ylabel('Fibre activation frecuency')
        self.ax3.set_title('Afferent fibres activity')

        # Iterate over each channel and plot
        for fibre_type, fibre_data in afferent_fibres_dict.items():
            data_to_plot = fibre_data['frequencies']
            x_axis_data = muscle_names
            self.ax3.plot(x_axis_data, data_to_plot, label=f'{fibre_type}')  # Plot each row

        # Set the x-axis limits
        self.ax3.set_xlim(min(x_axis_data), max(x_axis_data))

        # Set X-axis labels at 90 degrees
        self.ax3.tick_params(axis='x', rotation=90)

        # Place the legend outside the plot, to the right
        self.ax3.legend(loc='upper left', bbox_to_anchor=(1, 1))

        # Adjust the layout to make room for the legend
        self.fig.tight_layout()     

        # Redraw the plot
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

        return

    def plot_brain_region_activity(self, ch_array_list = [''], proxy_region_activity=['']):

        ch_list = np.where(np.isin(self.connectivity.region_labels, ch_array_list))[0]
        ch_list_proxy = np.where(np.isin(self.connectivity.region_labels, proxy_region_activity))[0]
        
        self.ax.clear()
        self.ax2.clear()

        # Set labels and titles as needed
        self.ax.set_xlabel('Simulation iteration')
        self.ax.set_ylabel('Brain activity')
        self.ax.set_title('Brain region acitivty')

        # Set labels and titles as needed
        self.ax2.set_xlabel('Simulation iteration')
        self.ax2.set_ylabel('Proxy region input voltage')
        self.ax2.set_title('Proxy region input voltage')

        # Iterate over each channel and plot
        for i, ch in enumerate(ch_list):
            data_to_plot = np.squeeze(self.results[0][1])[:, ch]
            x_axis_data = self.results[0][0]
            self.ax.plot(x_axis_data, data_to_plot, label=f'Region: {ch_array_list[i]}')  # Plot each row
        
        # Iterate over each channel and plot
        for i, ch in enumerate(ch_list_proxy):
            data_to_plot = np.squeeze(self.results[0][1])[:, ch]
            x_axis_data = self.results[0][0]
            self.ax2.plot(x_axis_data, data_to_plot, label=f'Muscle: {proxy_region_activity[i]}')  # Plot each row

        # Set the x-axis limits
        self.ax.set_xlim(min(x_axis_data), max(x_axis_data))
        self.ax2.set_xlim(min(x_axis_data), max(x_axis_data))

        # Place the legend outside the plot, to the right
        self.ax.legend(loc='upper left', bbox_to_anchor=(1, 1))

        # Place the legend for ax2
        lines, labels = self.ax2.get_legend_handles_labels()
        self.ax2.legend(lines[:5], labels[:5], loc='upper left')
        #self.ax2.legend(loc='upper left', bbox_to_anchor=(1, 1))

        # Adjust the layout to make room for the legend
        self.fig.tight_layout()     

        # Redraw the plot
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()


    def runLoop(self, timestep_ns):
        
        
        # Get afferent information
        type_Ia = self._getDataPack("type_Ia")
        type_II = self._getDataPack("type_II")
        type_Ib = self._getDataPack("type_Ib")

        afferent_fibres_dict = {'type_Ia': type_Ia, 'type_II': type_II, 'type_Ib': type_Ib}

        # Run the brain simulation
        # Data in tempres variable is sampled at a lower frequency, so it can't be used for setting targets

        commands, tempres = self.simulate_fun(self.simulator, type_Ia, type_II, type_Ib)

        # Save the results for plotting

        self.save_results(tempres, afferent_fibres_dict)

        # Set new targets for the fingers

        target = commands[1][-1, 0, 0, 0]

        self.target.append(target)
        self.w.append(commands[1][-1, 1, 0, 0])

        #self._setDataPack("action", { "act_list": [-3.0, -3.0, -3.0, target, target, target], "reset": 0})

        # Increment iteration and time variables

        self.time_ms += self.simulator.integrator.dt
        self.iteration += 1
        if(self.iteration%25==0):
        	print(str((self.iteration/100)/2)+" s")

# EOF
