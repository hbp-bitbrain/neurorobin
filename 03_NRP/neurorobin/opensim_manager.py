from nrp_core.engines.py_sim import PySimEngineScript

# The API of Opensim is shown in the following link:
# https://simtk.org/api_docs/opensim/api_docs

class Script(PySimEngineScript):
    def initialize(self):
        """Initialize datapack1 with time"""
        print("Server Engine is initializing")
        print("Registering datapack --> for sensors")
        self._registerDataPack("joints")
        self._setDataPack("joints", {"EM_tz": 0, "EF_y":0})
        self._registerDataPack("infos")
        self._setDataPack("infos", {"time": 0})
	
        self._registerDataPack("os_muscle_lengths")
        length_list = [0.0]*31
        self._setDataPack("os_muscle_lengths", {"length_list": length_list})
	
        velocity_list = [0.0]*31
        self._registerDataPack("os_muscle_velocities")
        self._setDataPack("os_muscle_velocities", {"velocity_list": velocity_list})
	
        force_list = [0.0]*31
        self._registerDataPack("os_muscle_forces")
        self._setDataPack("os_muscle_forces", {"force_list": force_list})
        self.force_list = force_list

        value_of_non_muscle = 0
        self.action = [0.1] * 38
        non_muscle_positions = range(31,38)
        self.action = [value_of_non_muscle if i in non_muscle_positions else self.action[i] for i in range(len(self.action))]
        
        print("Registering datapack --> for actuators")
        self._registerDataPack("control_cmd")
        self._registerDataPack("action")
        self._setDataPack("action", { "act_list": self.action, "reset": 0})
        
        ################# CONTROL ##########################
        self.state=0 #0: ir hacia adelante; 1: abrir la mano; 2: cerrar la mano
        self.count_go_return=0
        self.e_i = 0
        self.e_old = 0

        self.count = 0
        self.u = 0
        self.kp = 100#0.39
        self.ki = 0 #0.001
        self.kd = 0#0.99

        self.reset_count = 0
        
        joints = self._getDataPack("joints")
        exo_forward_value = joints.get("EM_tz")
        exo_fingers_value = joints.get("EF_y")
        self.initial_point=exo_forward_value ##### TAKE ZERO!!! #### INICIA EN -0.3813
        self.final_point=-0.43#back: -0.43#forward: -0.31
        #self.speed_target=0.5 #meters/sec
        self.step_period_sec=0.001

        min_rom=-0.59999999999999998 
        max_rom=0
        force_actuator=1000

        self.speed_old=0
        self.exo_base_position_old = self.initial_point
        self.exo_fingers_position_old = self.initial_point
        
        
        ##### SEQUENCE TO DO ######
        self.list_final_points_base=[-0.32, -0.37, -0.31, -0.43, -0.31, -0.43]
        self.list_final_points_fingers=[-0.6, 0.1]#[-17, 17, -17, 17, -17, 17, -17]
        
        #PID Base
        self.initial_point_base=0
        self.final_point_base=0
        self.speed_old_base=0
        self.speed_target_base=0.1 #0.1 meters/sec
        self.e_i_base = 0
        self.e_old_base = 0
        self.u_base = 0
        self.kp_base = 0.2#0.39
        self.ki_base = 0 #0.001
        self.kd_base = 0#0.99
        

        #PID Fingers
        self.initial_point_fingers=0
        self.final_point_fingers=0
        self.speed_old_fingers=0
        self.speed_target_fingers=1
        self.e_i_fingers = 0
        self.e_old_fingers = 0
        self.u_fingers = 0
        self.kp_fingers = 0.4#0.39
        self.ki_fingers = 0 #0.001
        self.kd_fingers = 0#0.99
        
        
        
        #self.commands=['go','return','go','return', 'go', 'open_hand','close_hand', 'open_hand','close_hand', 'open_hand','close_hand']
        self.commands=['go', 'open_hand','close_hand', 'return', 'open_hand','close_hand', 'go', 'open_hand','close_hand']
        self.actual_command=self.commands[0]
        self.actual_command_index=0
        self.old_command='f'
        
    def runLoop(self, timestep):
        # Receive control data from TF
        self.action = self._getDataPack("action").get("act_list")
        reset_falg = self._getDataPack("control_cmd").get("reset")
        
        ################### CONTROL ####################
        
        joints = self._getDataPack("joints")
         
        exo_base_position = joints.get("EM_tz")
        exo_fingers_position = joints.get("EF_y")
        if(self.actual_command!=self.old_command):
            # EXO BASE INITIALIZATION
            self.old_command=self.actual_command
            print("New command: "+self.commands[self.actual_command_index])
            if self.commands[self.actual_command_index]=='go':
            	#inicializamos go
            	#self.sim_manager.lock_coordinate("EF_y", True)
            	#self.sim_manager.lock_coordinate("EM_tz", True)
            	self.initial_point_base=exo_base_position
            	self.speed_target_base=0.1
            	self.final_point_base=self.list_final_points_base[0]
            	self.exo_base_position_old = self.initial_point_base
            	self.e_i_base = 0
            	self.e_old_base = 0
            	self.u_base = 0
            	self.kp_base = 16#0.2
            	self.ki_base = 0 #0.001
            	self.kd_base = 0#0.99
            	# EXO FINGERS INITIALIZATION
            	self.initial_point_fingers=exo_fingers_position
            	self.speed_target_fingers=1
            	self.final_point_fingers=exo_fingers_position#self.list_final_points_fingers[0]
            	self.exo_fingers_position_old = self.initial_point_fingers
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	self.u_fingers = 0
            	self.kp_fingers = 200#0.39
            	self.ki_fingers = 0 #0.001
            	self.kd_fingers = 0#0.99
            	
            	
            		
            elif self.commands[self.actual_command_index]=='return':
            	#inicializamos return
            	#self.sim_manager.lock_coordinate("EF_y", True)
            	#self.sim_manager.lock_coordinate("EM_tz", False)
            	self.count_go_return=self.count_go_return+1
            	self.state=3
            	self.final_point_base=self.list_final_points_base[1]
            	self.speed_target_base=-0.1
            	self.e_i_base = 0
            	self.e_old_base = 0
            	self.u_base = 0
            	self.kp_base = 120#0.39
            	self.ki_base = 0 #0.001
            	self.kd_base = 0#0.99
            	# EXO FINGERS INITIALIZATION
            	self.initial_point_fingers=exo_fingers_position
            	self.speed_target_fingers=1
            	self.final_point_fingers=exo_fingers_position#self.list_final_points_fingers[0]
            	self.exo_fingers_position_old = self.initial_point_fingers
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	self.u_fingers = 0
            	self.kp_fingers = 200#0.39
            	self.ki_fingers = 0 #0.001
            	self.kd_fingers = 0#0.99
            elif self.commands[self.actual_command_index]=='open_hand':
            	#inicializamos open hand
            	#self.sim_manager.lock_coordinate("EF_y", False)
            	#self.sim_manager.lock_coordinate("EM_tz", True)
            	self.state=1 #Abrir mano
            	self.final_point_base=exo_base_position
            	self.speed_target_base=-0.1
            	self.e_i_base = 0
            	self.e_old_base = 0
            	self.u_base = 0
            	self.kp_base = 120#0.39
            	self.ki_base = 0 #0.001
            	self.kd_base = 0#0.99
            	self.speed_target_fingers=-2
            	self.final_point_fingers=self.list_final_points_fingers[0]
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	self.u_fingers = 0
            	self.kp_fingers = 10
            	self.ki_fingers = 2
            	self.kd_fingers = 0
            elif self.commands[self.actual_command_index]=='close_hand':
            	#inicializamos close hand
            	#self.sim_manager.lock_coordinate("EF_y", False)
            	#self.sim_manager.lock_coordinate("EM_tz", True)
            	self.state=2 #Cerrar mano
            	self.final_point_base=exo_base_position
            	self.speed_target_base=-0.1
            	self.e_i_base = 0
            	self.e_old_base = 0
            	self.u_base = 0
            	self.kp_base = 120#0.39
            	self.ki_base = 0 #0.001
            	self.kd_base = 0#0.99
            	self.final_point_fingers=self.list_final_points_fingers[1]
            	self.speed_target_fingers=2
            	self.e_i_fingers = 0
            	self.e_old_fingers = 0
            	self.u_fingers = 0
            	self.kp_fingers = 10
            	self.ki_fingers = 2
            	self.kd_fingers = 0
            else:
            	print("Error: command not found")
            
            
        

        speed_actual_base=(exo_base_position-self.exo_base_position_old)/self.step_period_sec
        self.exo_base_position_old=exo_base_position
        #print("EXO_THUMB_POSITION")
        #print(exo_thumb_position)
        #print("EXO_FINGERS_POSITION")
        #print(exo_fingers_position)
        #print("EXO_THUMB_PSOITION_OLD")
        #print(self.exo_thumb_position_old)
        #print("Speed")
        speed_actual_fingers=(exo_fingers_position-self.exo_fingers_position_old)/self.step_period_sec
        self.exo_fingers_position_old=exo_fingers_position
        #print(speed_actual_thumb)
        #print(self.speed_target_thumb)
        if True:#self.count % 4 == 0:
            if self.commands[self.actual_command_index]=='go' or self.commands[self.actual_command_index]=='return':
            	err = speed_actual_base - self.speed_target_base
            	err = -err
            	self.u_base = self.pid_ctrl_base(err)
            	err = exo_fingers_position - self.final_point_fingers
            	#print("ERROR: "+str(err))
            	err = -err
            	self.u_fingers = self.pid_ctrl_fingers(err)
            else:
            	err = exo_base_position - self.final_point_base
            	err = -err
            	self.u_base = self.pid_ctrl_base(err)
            	err = speed_actual_fingers - self.speed_target_fingers
            	err = -err
            	self.u_fingers = self.pid_ctrl_fingers(err)

        self.count = self.count + 1

        reset_flag = 0
        self.reset_count = self.reset_count + 1
        
        if(self.u_base>200):
        	self.u_base=200
        elif(self.u_base<-200):
        	self.u_base=-200
        
        if self.commands[self.actual_command_index]=='go' and (self.reset_count>1):
        	if exo_base_position>self.final_point_base:
        		self.final_point_base=self.list_final_points_base[1]
        		if self.actual_command_index>=len(self.commands):
        			pass #Finalizamos
        		else:
        			self.actual_command_index=self.actual_command_index+1
        			self.actual_command=self.commands[self.actual_command_index]

        	self.action[31]=self.u_base
        	self.action[32]= self.u_fingers
        elif self.commands[self.actual_command_index]=='return' and (self.reset_count>1):
        	if exo_base_position<self.final_point_base:
        		self.final_point_base=self.list_final_points_base[0]
        		if self.actual_command_index>=len(self.commands):
        			pass #Finalizamos
        		else:
        			self.actual_command_index=self.actual_command_index+1
        			self.actual_command=self.commands[self.actual_command_index]

        	self.action[31]=self.u_base
        	self.action[32]= self.u_fingers
        elif self.commands[self.actual_command_index]=='open_hand'and (self.reset_count>1):
        	if exo_fingers_position<self.final_point_fingers:
        		self.final_point_fingers=self.list_final_points_fingers[1]
        		if self.actual_command_index>=len(self.commands):
        			pass #Finalizamos
        		else:
        			self.actual_command_index=self.actual_command_index+1
        			self.actual_command=self.commands[self.actual_command_index]
        	self.action[31]=self.u_base
        	if(self.u_fingers<-300):
        		self.u_fingers=-300
        	elif(self.u_fingers>0):
        		self.u_fingers=0
        	self.action[32]= self.u_fingers #-100
        elif self.commands[self.actual_command_index]=='close_hand' and (self.reset_count>1):
        	if exo_fingers_position>self.final_point_fingers:
        		self.final_point_fingers=self.list_final_points_fingers[0]
        		if self.actual_command_index>=len(self.commands):
        			pass #Finalizamos
        		else:
        			self.actual_command_index=self.actual_command_index+1
        			self.actual_command=self.commands[self.actual_command_index]
        	self.action[31]=self.u_base
        	self.action[32]= self.u_fingers#0.1

        self._setDataPack("action", { "act_list": self.action, "reset": reset_flag})


        if reset_falg == 1:
            print("RESET FALG")
            self.reset()
        else:
			# All Joints and Muscles can be found in the "*.osim"
            exo_forward_value = self.sim_manager.get_model_property("exotable1", "Joint")
            #exo_thumb_value = self.sim_manager.get_model_property("exothumb", datapack_type="Joint")
            exo_fingers_value = self.sim_manager.get_model_property("exomotorfingers", "Joint")
            # Send data to TF
            self._setDataPack("joints", {"EM_tz": exo_forward_value, "EF_y": exo_fingers_value})
            self._setDataPack("infos", {"time": self.sim_manager.get_sim_time()})
        # Set muscles' force to change joints
        self.sim_manager.run_step(self.action, timestep)
        #self.sim_manager.lock_coordinate("ET_z", True)

        self.force_list = self.sim_manager.get_model_properties("TendonForces")
        self._setDataPack("os_muscle_forces",{"force_list":self.force_list})
	
        length_list = self.sim_manager.get_model_properties("MuscleLengths")
        self._setDataPack("os_muscle_lengths", {"length_list": length_list})
	
        velocity_list = self.sim_manager.get_model_properties("MuscleSpeeds")
        self._setDataPack("os_muscle_velocities", {"velocity_list": velocity_list})

    def reset(self):
        print("resetting the opensim simulation...")
        # Reset the value of set datapacks
        self._setDataPack("joints", {"EM_tz": 0, "EF_y":0})
        self._setDataPack("infos", {"time": 0})
        # Reset simulation model
        self.sim_manager.reset()
        
    def shutdown(self):
        print("Engine 1 is shutting down")
        
    """
    def para_init(self):
        self.e_i_base = 0
        self.e_old_base = 0

        self.count = 0
        self.u_base = 0
        self.kp_base = 0.39
        self.ki_base = 0.001
        self.kd_base = 0.99
    """

    def pid_ctrl_base(self, error):
        self.e_i_base = self.e_i_base + error
        de = error - self.e_old_base
        u = self.kp_base*error + self.ki_base*self.e_i_base  + self.kd_base*de
        self.e_old_base = error

        return u
        

        
        
    def pid_ctrl_fingers(self, error):
        self.e_i_fingers = self.e_i_fingers + error
        de = error - self.e_old_fingers
        u = self.kp_fingers*error + self.ki_fingers*self.e_i_fingers  + self.kd_fingers*de
        self.e_old_fingers = error

        return u


