# NeuroRobin

## Description

This is the repository of Task 5.21 of the HBP: The objective of the task is to create a digital twin of an upper-limb neuro-rehabilitation session for a stroke patient that can help to improve and personalize this type of interventions by using advanced brain simulation tools that are able to exploit data collected from the patients.  To achieve this, T5.21 uses some of the tools developed in the Human Brain Project (HBP) [1], in particular, The Virtual Brain (TVB) [2] and the Neuro-Robotics Platform (NRP) [3] The main idea is to develop a closed loop simulator by:
1) using the TVB simulation tools to generate models of EEG activity of stroke patients. We will use data from stroke users including fMRI scans and EEG activity collected during rehabilitation tasks.
2) developing a complete biomechanical model for the upper-limb and a complete model of the ISMORE exoskeleton, as well as the motor primitives required for simulating neuro-rehabilitation intervention within the  NRP.
3) Integrate the previous simulation tools in a new use case in the NRP: “Closed-loop upper-limb neurorobot simulator for stroke neuro-rehabilitation”.
The final aim is to help the progress of scientific research in stroke, which is a high prevalence disease with a huge impact both in economic and social terms, by providing tools that can be used by the neurorehabilitation community to develop and evaluate interventions. All the models developed and to be developed are and will be open source and can be used for further experiments, in particular, the biomechanical models which can be used to control a real version of the ISMORE exo and the brain models to simulate brain activity during motor tasks after a stroke. We expect these tools will be of value mainly for research in neuro-rehabilitation, but they can also be used for basic research in motor control and for further development of a complete simulator of the nervous system. 

## Pre-processing
The pre-processing pipelines can be automatically executed using docker containers. However, this pipelines are not automatic when the patient has a brain lesion. In this case some extra steps have to be done. 

### Installation
The operating system used is Linux Ubuntu 20.04.
The pipelines are conteinarized in docker containers. Docker can be downloaded in the following link: https://www.docker.com/
Then, the pipelines can be intialized inside the following folder: neurorobin/01_Preprocessing

Additionally, if the user wants to observe the data or do custom pre-processing functions, the following neuroimaging tools have to be installed:

· MRtrix3: https://www.mrtrix.org/

· FSL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki

· FreeSurfer: https://surfer.nmr.mgh.harvard.edu/

· Brainstorm in Matlab: https://es.mathworks.com/products/matlab.html

· ITKSnap: http://www.itksnap.org/

### Usage
The different pipelines can be executed in order using the scripts on the folder "neurorobin/01_Preprocessing". The order is the following one: p1.sh, p2.sh and p3.sh.
The output from the different pipelines are stored in the output folder.

Executes MRtrix3 connectome pipeline, this creates the needed files to create a structural connectome:
```bash
sh p1.sh
```
Executes fMRIprep pipeline, this creates the needed files to see the BOLD activation on the different functional zones:
```bash
sh p2.sh
```
Executes TVB converter pipeline, this creates the needed files to be uploaded to the TVB platform and creates EEG projection and surfaces:
```bash
sh p3.sh
```
#### Pre-process stroke images
The different steps that have been done to adapt are described below:
##### Virtual brain transplant
The registration of the volumes when exists necrosed tissue in the brain do not work properly. This is due the registration techniques are adapted to a healthy brain.

A solution to that the T1 images can be a virtual brain transplant. Where the mirror healthy part of the brain is copied on top of the necrosed one. The different steps are:

· Segment the necrosed tissue, this can be done with ITKSnap or Freesurfer layer by layer.
· Flip the image.
· Copy the healthy tissue corresponding to the mirror part of the necrosed tissue
· Flip the image back again.
· Subtract the necrosed tissue
· Add the coppied healthy tissue to the blank necrosed tissue.

##### Distortion correction
In the case of this project, the distortion correction with phase reversed images could not be done due the lack of them. However, this project spin echo field maps to correct it.
The steps took towards this issue are going to be listed below. The main tool that allows this distortion correction is FSL FUGUE.
```bash
# Brain extraction
bet sub-TECNALIA_acq-DWI_magnitude.nii sub-TECNALIA_acq-DWI_magnitude_brain_extracted.nii -f 0.75
# Prepare field-map
fsl_prepare_fieldmap SIEMENS sub-TECNALIA_acq-DWI_phase.nii sub-TECNALIA_acq-DWI_magnitude_brain_extracted fieldmap 2.46
# Configure field-map with Gaussian Smoothing (s=0.5 standard)
fugue -i sub-TECNALIA_task-close_bold.nii -d 0.00246 --dwell=8.4e-06 --loadfmap=fieldmap_regu -u result
```
The values of each variable depend on the acquistion of the epi images, the values can be found in the json files and then the documentation from https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE has to be followed.
##### Stroke
To calculate the anatomically constrained tractography, the necrosed tissue that is 100% non-functional should be excluded from the DWI images. This is done due there might be some misscalculations of the tract fibres in that zones. However, the zones that are not 100% necrosed must remind in the original image because some tracts calculated might be still functional. 

##### BEM surfaces correction
When executing the tvb converter pipeline, there might be some errors regarding the BEM surface generation. The outer surfaces might intersect with the internal ones. An approach that lowers the lowest triangles from the outer surfaces is used.

Inside the neurorobin\01_Preprocessing\custom_scripts, the "correct_bem" function uses the surfaces imported from Brainstorm, which were previously imported from the tvb converter outputs inside the bem folder. 

##### Triangle correction
There are some cases that the freesurfer tools, more specifically the recon-all function generates some internal triangles inside the cortex surface. The bad vertices can be obtained from the validate topology of the TVB library once the cortex is imported.

Inside the neurorobin\01_Preprocessing\custom_scripts, the "surface_region_map_fixer" can be executed in order to correct the internal triangles. The input is the cortex.zip from the output of the tvb_converter pipeline and generates a new one with the cortex and region map fixed.

## The virtual brain

### Installation
The TVB simulator can be configured in Windows, Mac and Linux: https://www.thevirtualbrain.org/tvb/zwei/brainsimulator-software.
The data from the pipelines must be uploaded to the following folder: TVB_Windows_2.7.1\TVB_Distribution\tvb_data\Lib\site-packages\tvb_data
On the other hand, if the TVB GUI is being used, the data is imported inside the plaftorm.

### Usage
The data obtained from the pipelines can be used to simulate activity of the personalized brain. 
Moreover, there is a custom notebook for parameter optimization with the SJ3D neural mass model with the empirical functional connectome in neurorobin\02_TVB\2_Parameter_optimization\demo_scripts. The installation and code of the bayesian optimizer is in this directory: https://github.com/rmcantin/bayesopt 

## NeuroRobotics Platform

### Installation
The installation of the neurorobotics platform is automated with a scripts inside the 03_NRP folder. The file "nrp_initializer.sh" pulls the docker image of the NRP, copies the necessary files inside the docker in order to upload the experiment.

### Usage
Once the platform is initialized with the "nrp_initalizer", a command prompt will pop up. The initalizer automatically opens the experiment folder. In the experiment folder exists a file named "experiment_initializer". When it is executed the NRP initializes the OpenSim and TVB engines performing the closed-loop experiment.

The simulation_config.json file can be modified. The model is by default the stroke-affected paretic right limb.
```json
"PythonFileName": "opensim_manager.py",
"WorldFileName":"neurorobin_model/NeuroRobin.osim",
```
However, there can be executed experiments with a healthy limb, in this case the left limb pointing to the model.
```json
"PythonFileName": "opensim_manager_healthy.py",
"WorldFileName":"neurorobin_model/NeuroRobin_healthy.osim",
```
## Contributing
This project is open-source and is a base for future projects of upper-limb neurorehabilitation with TVB. New contributions will nourish this project, so they are appreciated.

Look at the documentation for the project execution. Follow the steps in order to contribute to the project.

## Authors and acknowledgment
BitBrain and Tecnalia.

Alex Escalera alex.escalera@bitbrain.com, Luis Montesano luis.montesano@bitbrain.com, Hector Lozano hector.lozano@tecnalia.com, Carlos Escolano carlos.escolano@bitbrain.com.

## License
Licence

## Project status
This project is currently under development.
