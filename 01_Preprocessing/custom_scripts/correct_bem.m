%Grab vertices of the outer surfaces
vertices = outer_skin.Vertices;
%Select the Z axis (3)
vertices_objective = vertices (:,3);
%Calculate height
height = max(vertices_objective) - min(vertices_objective);

%Calculate 10% height
height5percent = min(vertices_objective) + height*25/100;
%Create array of the vertices to move.
vertices_to_change = (vertices_objective < height5percent);

%Lower the vertices by -0.02
downed_vertices = vertices_objective - vertices_to_change/50;

outer_skin_corrected = outer_skin;

outer_skin_corrected.Vertices(:,3) = downed_vertices;

outer_skin_corrected.Comment = 'outer_skin_corrected';

%Grab vertices of the outer surfaces
vertices = outer_skull.Vertices;
%Select the Z axis (3)
vertices_objective = vertices (:,3);
%Calculate height
height = max(vertices_objective) - min(vertices_objective);

%Create array of the vertices to move.
vertices_to_change = (vertices_objective < height5percent);

%Lower the vertices by -0.02
downed_vertices = vertices_objective - vertices_to_change/50;

outer_skull_corrected = outer_skin;
outer_skull_corrected.Vertices(:,3) = downed_vertices;

outer_skull_corrected.Comment = 'outer_skull_corrected';