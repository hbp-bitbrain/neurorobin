%% Initial variables

bad_vertices_python = [1799,7755,8543,10116,11350,13645];
bad_vertices = bad_vertices_python + 1;

cd 'D:\Drive\BitBrain\NeuroRobin\2-TVB\2_Matlab\brainstorm_220831_src\brainstorm3\working folder'
%% LOAD CORTEX AND REGION MAPPING

cd files_2_work\input;
unzip('cortex.zip');
triangles = importdata('triangles.txt');
normals = importdata('normals.txt');
vertices = importdata('vertices.txt');
region_map = importdata('region_map.txt');

delete triangles.txt
delete normals.txt
delete vertices.txt
cd ..
cd ..
%%
pwd
load('pial_base.mat');
load('pial_base_big.mat');
pial.Faces = triangles + 1;
pial.Vertices = vertices;
pial.VertNormals = normals;


%% ADAPT REGION MAPPING TO CORTEX

n_regions = length(pial.Atlas(2).Scouts);
atlas = pial.Atlas(2).Scouts;

for region=1:n_regions
    pial.Atlas(2).Scouts(region).Vertices = [];
    atlas(region).Vertices = [];
end

region_map = region_map';
region_map = region_map +1;
for i = 1:length(region_map)
    target_region = region_map(i);
    atlas(target_region).Vertices = [atlas(target_region).Vertices, i];
end

pial.Atlas(2).Scouts = atlas;

%% CHANGE SOME VARIABLES FOR BRAINSTORM VISUALIZING

pial.VertConn = pial_base_big.VertConn(1:length(vertices), 1:length(vertices));
pial.Curvature = pial_base_big.Curvature(1:length(vertices));
pial.SulciMap = pial_base_big.SulciMap(1:length(vertices));

%% Fix other variables

vertices = pial.Vertices;
vertices(bad_vertices, :) = [];
pial.Vertices = vertices;

normals = pial.VertNormals;
normals(bad_vertices, :) = [];
pial.VertNormals = normals;

curvature = pial.Curvature;
curvature(bad_vertices) = [];
pial.Curvature = curvature;

sulci_map = pial.SulciMap;
sulci_map(bad_vertices) = [];
pial.SulciMap = sulci_map;

vert_conn = pial.VertConn;
vert_conn(bad_vertices,:) = [];
vert_conn(:,bad_vertices) = [];
pial.VertConn = vert_conn;

%% Fix faces

faces = pial.Faces;
errors = 0;
for i=1:length(bad_vertices)
    value = bad_vertices(i);
    position_of_triangle_bad_vertex = find(faces == value);
    if position_of_triangle_bad_vertex > length(faces)
        position_of_triangle_bad_vertex = position_of_triangle_bad_vertex - length(faces);
    end
    if position_of_triangle_bad_vertex > length(faces)
        position_of_triangle_bad_vertex = position_of_triangle_bad_vertex - length(faces);
    end
    n_triangles = length(position_of_triangle_bad_vertex);
    triangle_will_delete = faces(position_of_triangle_bad_vertex,:);
    if sum(triangle_will_delete==value) > 0
        faces(position_of_triangle_bad_vertex,:) = [];
    end
    if sum(triangle_will_delete==value) == 0
        errors = errors + 1;
    end  
end

for i=length(bad_vertices):-1:1
    binary_bigger_than_value = faces > bad_vertices(i);
    faces = faces - binary_bigger_than_value;
end

if max(faces)~=length(vertices)
    errors = errors + 1;
end
pial.Faces = faces;

%% Fix the region mapping for brainstorm

atlas = pial.Atlas(2).Scouts;
n_regions = length(atlas);
len_bad_vertices = length(bad_vertices);
bad_vertex_for_each_region = [];

for region_number=1:n_regions
    n_bad_vertex = 0;
    for j=len_bad_vertices:-1:1
        vertex_to_delete = bad_vertices(j);
        array_to_search = atlas(region_number).Vertices;
        position_of_bad_vertex = find(array_to_search == vertex_to_delete);
        if position_of_bad_vertex > 0
            n_bad_vertex = n_bad_vertex + 1;
            atlas(region_number).Vertices(position_of_bad_vertex) = [];
        end
    end
    bad_vertex_for_each_region = [bad_vertex_for_each_region,n_bad_vertex];
end
pial.Atlas(2).Scouts = atlas;


atlas = pial.Atlas(2).Scouts;
n_regions = length(atlas);
len_bad_vertices = length(bad_vertices);
for region_number=1:n_regions
    for j=len_bad_vertices:-1:1
        vertices_bigger_binary = atlas(region_number).Vertices >= bad_vertices(j);
        atlas(region_number).Vertices = atlas(region_number).Vertices - vertices_bigger_binary;
    end
end
pial.Atlas(2).Scouts = atlas;

pial_backup = pial;
n_regions = length(pial_backup.Atlas(2).Scouts);
overall_vertices = [];
for region=1:n_regions
    overall_vertices = [overall_vertices, pial_backup.Atlas(2).Scouts(region).Vertices];
end
%% Fix region map for TVB
region_map_backup = region_map;
region_map_backup(bad_vertices) = [];
region_map = region_map_backup;

%%
cd files_2_work\output

triangles = pial.Faces - 1;
normals = pial.VertNormals;
vertices = pial.Vertices;
region_map = region_map' - 1;

%%

save('triangles.txt', 'triangles', '-ASCII')
save('normals.txt', 'normals', '-ASCII')
save('vertices.txt', 'vertices', '-ASCII')
save('region_map_output.txt', 'region_map', '-ASCII')

cortex_files = ["triangles.txt", "normals.txt", "vertices.txt"];

zip('cortex_output.zip',cortex_files)

delete triangles.txt
delete normals.txt
delete vertices.txt

cd ..
cd ..
