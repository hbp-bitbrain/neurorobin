#!/bin/bash
# input_dir, folder with BIDS data set
input_dir=${PWD}"/input"

# output_dir, where to store results
output_dir=${PWD}"/output"

# participant_label, which subject to process from BIDS dataset
participant_label="TECNALIA"

# parcellation for SC and FC
parcellation="desikan"

# n_cpus, number of cpus for parallel processing
n_cpus=8
export OMP_NUM_THREADS=${n_cpus}
# mem_mb, memory for fmriprep in MB
mem_mb=10000

# freesurf_license, path to a freesurfer license for fmriprep
freesurf_license=${PWD}"/license.txt"

# name of task fmri, as in the BIDS_dataset
task_name="rest"

# create subfolder for pipeline to store results
mrtrix_output=${output_dir}"/mrtrix_output"
fmriprep_output=${output_dir}"/fmriprep_output"
fmriprep_workdir=${fmriprep_output}"/tmp"
tvb_output=${output_dir}"/TVB_output"
tvb_workdir=${tvb_output}"/tmp"
mkdir -p ${mrtrix_output} ${fmriprep_output} ${fmriprep_workdir} ${tvb_output} ${tvb_workdir}


###########################################################################################
###########################################################################################
###########################################################################################
# run mrtrix3 connectome to get a structural connectome
# the following command run the bids/mrtrix3_connectome
# a short workaround is implemented to overwrite the cleanup setting of mrtrix3_connectome.py script
# this way the freesurfer recon-all results are not deleted after the pipeline has finsihed
docker run -it -v ${input_dir}:${input_dir} \
        -v ${mrtrix_output}:${mrtrix_output} \
        --entrypoint python \
		bids/mrtrix3_connectome:0.4.2 -c "from mrtrix3 import app; app.cleanup=False; \
        import sys; sys.argv='/mrtrix3_connectome.py ${input_dir} ${mrtrix_output} participant \
        --participant_label ${participant_label} --parcellation ${parcellation} \
        --output_verbosity 3 --skip-bids-validator --template_reg ants --n_cpus ${n_cpus} --debug \
        '.split(); execfile('/mrtrix3_connectome.py')"
