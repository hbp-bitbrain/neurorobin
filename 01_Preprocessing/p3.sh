#!/bin/bash
# input_dir, folder with BIDS data set
input_dir=${PWD}"/input"

# output_dir, where to store results
output_dir=${PWD}"/output"

# participant_label, which subject to process from BIDS dataset
participant_label="TECNALIA"

# parcellation for SC and FC
parcellation="desikan"

# n_cpus, number of cpus for parallel processing
n_cpus=8
export OMP_NUM_THREADS=${n_cpus}
# mem_mb, memory for fmriprep in MB
mem_mb=10000

# freesurf_license, path to a freesurfer license for fmriprep
freesurf_license=${PWD}"/license.txt"

# name of task fmri, as in the BIDS_dataset
task_name="close"

# create subfolder for pipeline to store results
mrtrix_output=${output_dir}"/mrtrix_output"
fmriprep_output=${output_dir}"/fmriprep_output"
fmriprep_workdir=${fmriprep_output}"/tmp"
tvb_output=${output_dir}"/TVB_output"
tvb_workdir=${tvb_output}"/tmp"
mkdir -p ${mrtrix_output} ${fmriprep_output} ${fmriprep_workdir} ${tvb_output} ${tvb_workdir}


###########################################################################################
###########################################################################################
###########################################################################################
mrtrix_recon_all_dir=`find ${mrtrix_output} -name "mrtrix3_connectome.py*" -type d`
mrtrix_recon_all_name="freesurfer"
# run TVBconverter to generate TVB format data, additionally compute the EEG leadfield matrix

# mrtrix_recon_all_dir is set
recon_all_dir=${mrtrix_recon_all_dir}
recon_all_subject_name=${mrtrix_recon_all_name}

weighs_path=${mrtrix_output}"/sub-"${participant_label}"/connectome/sub-"${participant_label}"_parc-"${parcellation}"_level-participant_connectome.csv"
tracts_path=${mrtrix_output}"/sub-"${participant_label}"/connectome/sub-"${participant_label}"_parc-"${parcellation}"_meanlength.csv"

docker run -it --rm \
	-v ${output_dir}:${output_dir} \
	-v ${input_dir}:${input_dir} \
	-v ${freesurf_license}:/opt/freesurfer/license.txt \
	thevirtualbrain/tvb_converter:latest ${input_dir} ${output_dir} ${mrtrix_output} ${fmriprep_output} ${fmriprep_workdir} \
	${tvb_output} ${tvb_workdir} ${recon_all_dir} ${recon_all_subject_name} ${participant_label} ${task_name} \
	${parcellation} ${weighs_path} ${tracts_path} ${n_cpus} \
	
