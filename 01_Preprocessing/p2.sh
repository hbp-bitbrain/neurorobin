#!/bin/bash
# input_dir, folder with BIDS data set
input_dir=${PWD}"/input"

# output_dir, where to store results
output_dir=${PWD}"/output"

# participant_label, which subject to process from BIDS dataset
participant_label="TECNALIA"

# parcellation for SC and FC
parcellation="desikan"

# n_cpus, number of cpus for parallel processing
n_cpus=8
export OMP_NUM_THREADS=${n_cpus}
# mem_mb, memory for fmriprep in MB
mem_mb=10000

# freesurf_license, path to a freesurfer license for fmriprep
freesurf_license=${PWD}"/license.txt"

# name of task fmri, as in the BIDS_dataset
task_name="rest"

# create subfolder for pipeline to store results
mrtrix_output=${output_dir}"/mrtrix_output"
fmriprep_output=${output_dir}"/fmriprep_output"
fmriprep_workdir=${fmriprep_output}"/tmp"
tvb_output=${output_dir}"/TVB_output"
tvb_workdir=${tvb_output}"/tmp"
mkdir -p ${mrtrix_output} ${fmriprep_output} ${fmriprep_workdir} ${tvb_output} ${tvb_workdir}


###########################################################################################
###########################################################################################
###########################################################################################
# run fmriprep to preprocess fmri data
# use the freesurfer recon-all results from mrtrix3_connectome container if available
# only these parcellations run recon-all in the mrtrix3_connectome pipeline
# find recon-all results in temporary folder and copy them to fmriprep_output
# mrtrix_recon_all_dir=`find ${mrtrix_output} -name "mrtrix3_connectome.py*" -type d`
# mrtrix_recon_all_name="freesurfer"
#mkdir ${fmriprep_output}"/freesurfer"
#cp -r "/home/alex/code/tecndata/output/mrtirx_output/freesurfer/mrtrix3_connectome.py-tmp-EK51PT/freesurfer" ${fmriprep_output}"/freesurfer/sub-"${participant_label}

docker run -i --rm\
		-v ${input_dir}:${input_dir} \
        -v ${fmriprep_output}:${fmriprep_output} \
        -v ${fmriprep_workdir}:${fmriprep_workdir} \
        -v ${freesurf_license}:/opt/freesurfer/license.txt \
        poldracklab/fmriprep:20.1.1 \
		${input_dir} ${fmriprep_output} participant --bold2t1w-dof 6 --use-aroma --output-spaces T1w MNI152NLin6Asym:res-2 fsaverage5 --participant_label ${participant_label}  -w ${fmriprep_workdir} --low-mem
